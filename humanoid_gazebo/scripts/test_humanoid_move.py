#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64
import math

def main():
    rospy.init_node("q_set_control_node")

    l_hip_pitch_position = rospy.Publisher('humanoid/l_hip_pitch_position/command', Float64, queue_size=10)
    l_hip_roll_position = rospy.Publisher('humanoid/l_hip_roll_position/command', Float64, queue_size=10)
    l_hip_yaw_position = rospy.Publisher('humanoid/l_hip_yaw_position/command', Float64, queue_size=10)

    l_knee_pitch_position = rospy.Publisher('humanoid/l_knee_pitch_position/command', Float64, queue_size=10)

    l_foot_pitch_position = rospy.Publisher('humanoid/l_foot_pitch_position/command', Float64, queue_size=10)
    l_foot_roll_position = rospy.Publisher('humanoid/l_foot_roll_position/command', Float64, queue_size=10)

    r_hip_pitch_position = rospy.Publisher('humanoid/r_hip_pitch_position/command', Float64, queue_size=10)
    r_hip_roll_position = rospy.Publisher('humanoid/r_hip_roll_position/command', Float64, queue_size=10)
    r_hip_yaw_position = rospy.Publisher('humanoid/r_hip_yaw_position/command', Float64, queue_size=10)

    r_knee_pitch_position = rospy.Publisher('humanoid/r_knee_pitch_position/command', Float64, queue_size=10)

    r_foot_pitch_position = rospy.Publisher('humanoid/r_foot_pitch_position/command', Float64, queue_size=10)
    r_foot_roll_position = rospy.Publisher('humanoid/r_foot_roll_position/command', Float64, queue_size=10)

    rate = rospy.Rate(10)

    pos = -math.pi/2
    inc = 0.1
    curr_inc = inc

    while not rospy.is_shutdown():

        if pos > 0:
            curr_inc = -inc
        if pos < -math.pi/2:
            curr_inc = inc

        r_hip_pitch_position.publish(pos)
        l_hip_pitch_position.publish(pos)

        rate.sleep()

        pos = pos + curr_inc

        print(pos)
        print(curr_inc)

if __name__ == '__main__':
    main()
