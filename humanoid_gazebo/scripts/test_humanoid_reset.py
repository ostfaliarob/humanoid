#!/usr/bin/env python

import rospy
from std_msgs.msg import Float64
import math

def main():
    rospy.init_node("q_set_control_node")

    l_hip_pitch_position = rospy.Publisher('humanoid/l_hip_pitch_position/command', Float64, queue_size=10)
    l_hip_roll_position = rospy.Publisher('humanoid/l_hip_roll_position/command', Float64, queue_size=10)
    l_hip_yaw_position = rospy.Publisher('humanoid/l_hip_yaw_position/command', Float64, queue_size=10)

    l_knee_pitch_position = rospy.Publisher('humanoid/l_knee_pitch_position/command', Float64, queue_size=10)

    l_foot_pitch_position = rospy.Publisher('humanoid/l_foot_pitch_position/command', Float64, queue_size=10)
    l_foot_roll_position = rospy.Publisher('humanoid/l_foot_roll_position/command', Float64, queue_size=10)

    r_hip_pitch_position = rospy.Publisher('humanoid/r_hip_pitch_position/command', Float64, queue_size=10)
    r_hip_roll_position = rospy.Publisher('humanoid/r_hip_roll_position/command', Float64, queue_size=10)
    r_hip_yaw_position = rospy.Publisher('humanoid/r_hip_yaw_position/command', Float64, queue_size=10)

    r_knee_pitch_position = rospy.Publisher('humanoid/r_knee_pitch_position/command', Float64, queue_size=10)

    r_foot_pitch_position = rospy.Publisher('humanoid/r_foot_pitch_position/command', Float64, queue_size=10)
    r_foot_roll_position = rospy.Publisher('humanoid/r_foot_roll_position/command', Float64, queue_size=10)

    l_pub_list = [l_hip_pitch_position, l_hip_roll_position, l_hip_yaw_position, l_knee_pitch_position, l_foot_pitch_position, l_foot_roll_position]
    r_pub_list = [r_hip_pitch_position, r_hip_roll_position, r_hip_yaw_position, r_knee_pitch_position, r_foot_pitch_position, r_foot_roll_position]

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():

        for l_pub in l_pub_list:
            l_pub.publish(0)
            rate.sleep()

        for r_pub in r_pub_list:
            r_pub.publish(0)
            rate.sleep()

if __name__ == '__main__':
    main()
