# Humanoid Robot

<img alt="modell" src="images/modell.jpg" width="400">

<img alt="move" src="images/move.jpg" width="400">

## Installing

### 1. Create workspace
```
mkdir -p ~/humanoid_ws/src
```

### 2. Clone humanoid repository
```
cd ~/humanoid_ws/src
git clone https://gitlab.com/ostfaliarob/humanoid.git
```

### 3. Make
```
cd ~/humanoid_ws/
catkin_make
```

### 4. Demo
```
source ~/humanoid_ws/devel/setup.bash
roslaunch humanoid_moveit demo.launch
```

## Useful commands

### Open urdf model in RViz
```
roslaunch humanoid_description rviz.launch
```

### Open urdf model in Gazebo
```
roslaunch humanoid_simulation humanoid_demo.launch
```

### Run python script
```
rosrun humanoid_gazebo test_humanoid_move.py
```

### Convert xacro file to urdf file
```
rosrun xacro xacro --inorder humanoid.urdf.xacro > humanoid.urdf
```

### Spawn urdf model in Gazebo
```
rosrun gazebo_ros spawn_model -file humanoid.urdf -urdf -z 1 -x 1 -model test1
```
